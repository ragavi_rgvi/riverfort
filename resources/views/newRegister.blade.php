<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Register</title>
    <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <!-- Custom fonts for this template-->
  <!-- Custom fonts for this template-->
    <link href="{{ asset('customlogin/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('customlogin/css/sb-admin-2.min.css')}}" rel="stylesheet">

</head>

<body class="bg-gradient-primary" style="background: url('https://lithub.com/wp-content/uploads/2019/07/books.jpg')">
    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block"> 
                        <img height="100%" width="100%" src="https://www.riverfortacademy.sg/wp-content/uploads/2020/10/joey-alleyway-2-scaled.jpg">
                    </div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                            </div>
                            <form  class = "user" method="POST" action="{{ route('register') }}">
                            @csrf 
                                <div class="form-group row ">
                                    <div class="col-sm-3 mb-4 mb-sm-0">
                                        &nbsp;&nbsp;&nbsp;
                                        <label for="gender">Role</label>
                                    </div>
                                    <div class="col-sm-3 mb-4 mb-sm-0 ">
                                       <div class="custom-control custom-radio small">                                                 
                                            <input class="custom-control-input role" type="radio" name="role" id="customCheckteacher" value="1" checked="true" }}>
                                            <label class="custom-control-label" for="customCheckteacher"><p>Teacher</p>
                                            </label>
                                        </div>
                                    </div>
                                     <div class="col-sm-3 mb-4 mb-sm-0 ">
                                        <div class="custom-control custom-radio small">
                                            <input class="custom-control-input role " type="radio" name="role" id="customCheckstudent"  value="2"}}>
                                            <label class="custom-control-label" for="customCheckstudent">Student
                                            </label>
                                        </div>
                                    </div>
                                     <div class="col-sm-3 mb-4 mb-sm-0">
                                        <div class="custom-control custom-radio small">
                                            <input class="custom-control-input role " type="radio" name="role" id="customCheckparent"  value = "3" }}>
                                            <label class="custom-control-label medium" for="customCheckparent">Parent
                                            </label>
                                        </div>
                                    </div>                                    
                                </div>
                                <div class="form-group row" >
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <input id="name" type="text" class="form-control form-control-user @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="User Name">
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror                                        
                                    </div>                                    
                                </div>
                                <div class="form-group">
                                    <input id="email" type="email" class="form-control form-control-user @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email Address" required autocomplete="email">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror                                    
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror form-control-user" name="password" placeholder="Password" required autocomplete="new-password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col-sm-6">
                                        <input  id="password-confirm" type="password" class="form-control form-control-user" name="password_confirmation" required autocomplete="new-password"
                                         placeholder="Repeat Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input id="fullname" type="text" class="form-control form-control-user" autofocus  name="fullname"  placeholder="Full name" required >                                                                     
                                </div>





                                <div class="form-group "  id="student">
                                    <input id="rollid" type="text" class="form-control form-control-user"  autofocus name="rollid"  placeholder="Roll Number" >                                                                     
                                </div>
                                <div class="form-group row " id="student"> 
                                   <!--  <div  class="col-sm-2 mb-4 mb-sm-0">
                                        <label for='class'>class</label>
                                    </div>   -->                               
                                    <div  class="col-sm-12 mb-4 mb-sm-0" > 
                                        <select class="form-control dropdown-item-text  " name="class" id="class" placeholder="select Class" autofocus >
                                            <option value = ''>Select Class</option>
                                            @foreach ($classes as $classes)
                                                <option  value = '{{ $classes->id }}'>{{$classes->className}}</option>                                                 
                                            @endforeach 
                                        </select> 
                                    </div>
                                    
                                                                                                       
                                                                                                    
                                </div>
                                <div class="form-group row "  id="student"> 
                                   <!--  <div  class="col-sm-2 mb-4 mb-sm-0">
                                        <label for='class'>class</label>
                                    </div>   -->                               
                                    <div  class="col-sm-12 mb-4 mb-sm-0" > 
                                        <select class="form-control dropdown-item-text" name="Section" id="section" placeholder="select Section" autofocus  >
                                            <option value = ''>Select Section</option>
                                            @foreach ($sections as $sections)
                                                <option data_id ='{{ $sections->classId }}' value = '{{ $sections->classId }}'>{{$sections->sectionName}}</option>                                           
                                            @endforeach                                           
                                        </select> 
                                    </div>
                                    
                                                                                                       
                                                                                                    
                                </div>

                                <div class="form-group " id="parent" >
                                    <input id="profession" type="text" class="form-control form-control-user"  name="profession"  placeholder="Profession" >
                                </div>

                                <div class="form-group">
                                    <input id="birthday" type="text" class="form-control form-control-user"  name="birthday"  placeholder="Date of birth" required >
                                </div>
                                
                                 <div class="form-group row">
                                    <div class="col-sm-4 mb-4 mb-sm-0">
                                        &nbsp;&nbsp;&nbsp;
                                        <label for="gender">Gender</label>
                                    </div>
                                    <div class="col-sm-4 mb-4 mb-sm-0">
                                        <div class="custom-control custom-radio small">
                                            <input class="custom-control-input " type="radio" name="gender" id="customCheckmale"  value="male" }}>
                                            <label class="custom-control-label" for="customCheckmale">Male
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 mb-4 mb-sm-0">
                                        <div class="custom-control custom-radio small">
                                            <input class="custom-control-input " type="radio" name="gender" id="customCheckfemale" value="female"  }}>
                                            <label class="custom-control-label medium" for="customCheckfemale">Female
                                            </label>
                                        </div>
                                    </div>                                    
                                </div>
                                <br>
                                 <div class="form-group">
                                    <input id="address" type="text" class="form-control form-control-user"  name="address"  placeholder="Address" required >                                                                     
                                </div>
                                <div class="form-group">
                                    <input id="phone" type="tel" class="form-control form-control-user" pattern="[0-9]{10}"  name="phone"  placeholder="Phone Number" required >                                                                     
                                </div>
                                <div class="form-group row parent" id="parent">
                                    <div class="col-sm-4 mb-4 mb-sm-0">
                                        &nbsp;&nbsp;&nbsp;
                                        <label for="link">Parent of</label>
                                    </div>
                                    <div class="col-sm-4 mb-4 mb-sm-0">
                                        <button name= "link" type="button" class="btn btn-primary btn-block btn-sm">
                                           Link Student
                                        </button>
                                    </div>
                                </div> 

                                <button type="submit" class="btn btn-user btn-block btn-primary">
                                    {{ __('Register') }}
                                </button>                               
                            </form>
                            <hr>
                            
                            <div class="text-center">
                                <a class="small" href="{{ route('login') }}">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
<script>
    //
    $( document ).ready(function() {       
       if($('#customCheckteacher').is(":checked") === true){           
           $('div #student').addClass('d-none');
           $('div #parent').addClass('d-none');
       }   
    });
    $('#customCheckstudent').click(function(){
        $('div #parent').addClass('d-none');
        $('div #student').removeClass('d-none');
    });
    $('#customCheckparent').click(function(){
        $('div #parent').removeClass('d-none');
        $('div #student').addClass('d-none');
    });
    $('#customCheckteacher').click(function(){
        $('div #parent').removeClass('d-none');
        $('div #student').removeClass('d-none');
    });
    $('#class').change(function(){
        var selectedClass = $('#class option:selected').val();        
        console.log(selectedClass);
        var select = $('#section option');        
        $("#section option").hide(); 
         $("#section ").val('').show();          
        $.each(select ,function(key, value) {                
            if($(this).val() == selectedClass){               
                $(this).show();                    
            }               
        });     
    });    
</script>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>